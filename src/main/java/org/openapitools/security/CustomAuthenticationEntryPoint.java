package org.openapitools.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.openapitools.model.Error;
import org.openapitools.model.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.time.OffsetDateTime;
import java.util.UUID;

@Component("customAuthenticationEntryPoint")
public class CustomAuthenticationEntryPoint implements AuthenticationEntryPoint {

    @Autowired
    ObjectMapper mapper;

    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException)
            throws IOException, ServletException {

        Error error = new Error()
                .responseHeader(new ResponseHeader()
                        .requestId(UUID.randomUUID())
                        .sendDate(OffsetDateTime.now()))
                .code("Unauthorized")
                .message("Unauthorized");

        response.setContentType(MediaType.APPLICATION_JSON_VALUE);
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        OutputStream responseStream = response.getOutputStream();
        mapper.writeValue(responseStream, error);
        responseStream.flush();
    }
}
