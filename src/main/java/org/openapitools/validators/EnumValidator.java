package org.openapitools.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class EnumValidator implements ConstraintValidator<EnumConstraint, CharSequence> {

    private Class<? extends Enum<?>> enumClass;
    private boolean ignoreCase;

    @Override
    public void initialize(EnumConstraint constraintAnnotation) {
        enumClass = constraintAnnotation.enumClass();
        ignoreCase = constraintAnnotation.ignoreCase();
    }

    @Override
    public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
        if (value == null) {
            return true; // null values are considered valid
        }

        for (Enum<?> enumValue : enumClass.getEnumConstants()) {
            if (ignoreCase) {
                if (enumValue.name().equalsIgnoreCase(value.toString())) {
                    return true;
                }
            } else {
                if (enumValue.name().equals(value.toString())) {
                    return true;
                }
            }
        }

        return false;
    }
}